class_name Hole
extends Interactable

onready var collision_shape := $CollisionShape2D
onready var animation_player := $AnimationPlayer

func _ready() -> void:
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.connect("dice_unselected", self, "_on_dice_unselected")

func use_dice(dice: DiceTextureRect) -> bool:
	var new_dice = load("Dice/Dice.tscn").instance()
	new_dice.dice_resource = dice.dice_resource
	add_child(new_dice)
	collision_shape.disabled = true
	EventBus.emit_signal("dice_used", {"instance": self})
	return true

func _on_dice_selected(msg) -> void:
	animation_player.play("dice_selected")

func _on_dice_unselected(msg) -> void:
	animation_player.stop()
	animation_player.play("RESET")

func enable_collision() -> void:
	collision_shape.set_deferred("disabled", false)
