extends Panel

var dice

onready var DiceTexture := preload("res://Dice/DiceTextureRect.tscn")

onready var rolls_remaining_label := $RollsRemaining
onready var dice_container := $GridContainer
onready var item_container := owner.get_node("ItemsCollection/GridContainer")

func _unhandled_key_input(event: InputEventKey) -> void:
	input_test_select(event)
	if Input.is_action_just_pressed("roll_dice", true):
		EventBus.emit_signal("roll_dice")

func input_test_select(event: InputEventKey) -> void:
	for i in range(1, 9):
		if Input.is_action_just_pressed("select_%s" % i, true):
			select_dice(i - 1)
		if Input.is_action_just_pressed("select_item_%s" % i, true):
			select_item(i - 1)

func select_dice(dice_num: int) -> void:
	var die_arr = dice_container.get_children()
	if die_arr.size() > dice_num:
		var dice_texture = die_arr[dice_num] as DiceTextureRect
		if dice_texture:
			dice_texture.select()

func select_item(item_num: int) -> void:
	var item_arr = item_container.get_children()
	if item_arr.size() > item_num:
		var item_texture = item_arr[item_num] as ItemTextureRect
		if item_texture:
			item_texture.select()

func _ready() -> void:
	EventBus.connect("remaining_dice_changed", self, "_on_remaining_dice_changed")

func add_dice(side: int) -> void:
	pass

func _on_remaining_dice_changed(rolls: int, dice_resource: Resource = null) -> void:
	if dice_resource:
		var dice_texture = DiceTexture.instance()
		dice_texture.dice_resource = dice_resource
		dice_texture._set_side(dice_resource.side)
		dice_container.add_child(dice_texture)
		rolls_remaining_label.text = "Rolls remaining: %d" % rolls
	else:
		rolls_remaining_label.text = "Rolls remaining: %d" % (rolls)
	if rolls == 0:
		$Roll/RollDiceButton.disabled = true

func _on_RollDiceButton_pressed() -> void:
	EventBus.emit_signal("roll_dice")
