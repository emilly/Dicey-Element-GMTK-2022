class_name Player
extends Area2D

var speed: int = 16
var movement_direction := Vector2.ZERO
var last_movement_direction := Vector2.ZERO
var is_sliding := false
var can_slide := false

onready var sprite := $Sprite
onready var raycast := $RayCast2D
onready var ray_next_tile := $RayNextTile
onready var collision_shape := $CollisionShape2D
onready var animation_player := $AnimationPlayer
onready var movement_player := $MovementPlayer


onready var dice_holder := $Sprite/DiceHolder

var movement_side := 0

func _ready() -> void:
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.connect("dice_unselected", self, "_on_dice_unselected")

func _physics_process(delta: float) -> void:
	movement_direction = update_movement_direction()
	var original_position = raycast.position
	var ray_info = check_next_tile(movement_direction)
	if ray_info:
		if can_slide and ray_info[0].collider is IceArea:
			if not is_sliding:
				last_movement_direction = movement_direction
			is_sliding = true
			ray_info = check_next_tile(last_movement_direction)
			var ray_info_collider = ray_info[0].collider if ray_info else null
			if ray_info_collider and not ray_info_collider is IceArea:
				is_sliding = false
				can_slide = false
				return
			else:
				raycast.position += last_movement_direction * speed
	if (not ray_info || not (ray_info[0].collider is IceArea) and ray_info[0].collider is Area2D):
		is_sliding = false
		raycast.position += movement_direction * speed
	collision_shape.position = raycast.position
	sprite.position = lerp(sprite.position, raycast.position, 0.2)
	if original_position != raycast.position && movement_direction != Vector2.ZERO:
		remove_dice_side()
		print("next_turn")
		EventBus.emit_signal("next_turn")
		movement_player.play()
		update_dice_texture()

func update_movement_direction() -> Vector2:
	var new_movement_direction := Vector2(
		(
			-int(Input.is_action_just_pressed("move_left")) + 
			int(Input.is_action_just_pressed("move_right"))
		),
		(
			-int(Input.is_action_just_pressed("move_up")) +
			int(Input.is_action_just_pressed("move_down"))
		)
	)
	if new_movement_direction != Vector2.ZERO && movement_side:
		last_movement_direction = new_movement_direction
		can_slide = true
	else:
		return Vector2.ZERO
	return new_movement_direction

func get_global_position() -> Vector2:
	return raycast.global_position

func get_dice_holder_position() -> Vector2:
	return dice_holder.rect_global_position

func _on_dice_selected(msg) -> void:
	animation_player.play("dice_selected")

func _on_dice_unselected(msg) -> void:
	animation_player.stop()
	animation_player.play("RESET")

func die() -> void:
	$DeathPlayer.play()
	Transition.reload_current_scene()

func slide() -> void:
	pass

func check_next_tile(direction := movement_direction) -> Dictionary:
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_point(
		raycast.global_position + (direction * speed),
		32,
		[self],
		1,
		true,
		true)
	return result

func add_dice(dice: DiceTextureRect) -> void:
	if dice.dice_resource.side == DiceResource.Special.INFINITE:
		movement_side = 7
	else:
		movement_side = min(movement_side + dice.dice_resource.side, 6)
	update_dice_texture()

func update_dice_texture() -> void:
	if movement_side == 0:
		dice_holder.self_modulate = Color(1, 1, 1, 1)
	else:
		dice_holder.self_modulate = Color(1, 1, 1, .5)
	if movement_side == DiceResource.Special.INFINITE:
		dice_holder.texture = load("Dice/dice_infinite.png")
	else:
		dice_holder.texture = load("Dice/dice_%d.png" % movement_side)

func remove_dice_side() -> void:
	if movement_side < 7:
		movement_side -= 1
