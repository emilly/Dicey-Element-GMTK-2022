class_name Level
extends Node2D

enum TileID {
	NONE = -1
	FLOOR = 0
	WALL = 1
	EMPTY = 2
}

export(String, FILE) var next_level
export(Array, Resource) var dices: Array
export var level0_tutorial := false
export var hole_tutorial := false
export var water_tutorial := false
export var fire_tutorial := false
export var keys_tutorial := false
export var spikes_tutorial := false
export var diagonals_tutorial := false

var rng := RandomNumberGenerator.new()
var dice_selected := false
var current_dice: DiceTextureRect
var current_item: ItemTextureRect
var items: Dictionary
var interactables: Dictionary

onready var rolls := dices.size()
onready var level_tilemap := $LevelTileMap
onready var player := $Player

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("selected"):
		if dice_selected:
			var tile_position = tile_mouse_position()
			var interactable = interactables.get(tile_position)
			if is_instance_valid(interactable):
				var has_used_dice = interactable.use_dice(current_dice)
				if has_used_dice:
					remove_dice(current_dice)
			else:
				place_dice(current_dice)
				
		if current_item:
			var tile_position = tile_mouse_position()
			var interactable = interactables.get(tile_position)
			if is_instance_valid(interactable):
				interactable.use_item(current_item)
	if Input.is_action_just_pressed("reload_level"):
		Transition.reload_current_scene()

func _ready() -> void:
	for item in get_tree().get_nodes_in_group("Item"):
		var item_tile_position = snap_vector_to_tile(item.global_position)
		items[item_tile_position] = item
	for interactable in get_tree().get_nodes_in_group("Interactable"):
		var interactable_tile_position = snap_vector_to_tile(interactable.global_position)
		interactables[interactable_tile_position] = interactable
	EventBus.connect("roll_dice", self, "_on_roll_dice")
	EventBus.emit_signal("remaining_dice_changed", rolls)
	EventBus.connect("dice_select", self, "_on_dice_select")
	EventBus.connect("change_level", self, "_on_change_level")
	EventBus.connect("select_item", self, "_on_select_item")
	EventBus.connect("dice_destroyed", self, "_on_dice_destroyed")
	EventBus.connect("dice_unselect", self, "_on_dice_unselect")
	player.dice_holder.connect("gui_input", self, "_on_player_dice_holder_gui_input")
	
	if level0_tutorial:
		EventBus.emit_signal("tutorial_roll_dice")
	if hole_tutorial:
		EventBus.emit_signal("tutorial_hole")
	if water_tutorial:
		EventBus.emit_signal("tutorial_water")
	if fire_tutorial:
		EventBus.emit_signal("tutorial_fire")
	if keys_tutorial:
		EventBus.emit_signal("tutorial_keys")
	if spikes_tutorial:
		EventBus.emit_signal("tutorial_spikes")
	if diagonals_tutorial:
		EventBus.emit_signal("tutorial_diagonals")

func _on_roll_dice() -> void:
	if rolls > 0:
		var dice_side = dices.pop_front()
		rolls -= 1
		EventBus.emit_signal("remaining_dice_changed", rolls, dice_side)

func _on_dice_select(msg: Dictionary):
	dice_selected = true
	current_dice = msg.instance
	EventBus.emit_signal("dice_selected", msg)

func tile_mouse_position() -> Vector2:
	return (get_global_mouse_position() / level_tilemap.cell_size).floor()

func player_tile_position() -> Vector2:
	return (player.get_global_position() / level_tilemap.cell_size).floor()

func dice_movement(dice: DiceTextureRect) -> void:
	player.movement_side += dice.dice_resource.side
	remove_dice(dice)

func _on_change_level() -> void:
	Transition.change_scene(next_level)

func place_dice(dice: DiceTextureRect) -> bool:
	var tile_position = tile_mouse_position()
	var tile_id = level_tilemap.get_cell(tile_position.x, tile_position.y)
	match tile_id:
		TileID.EMPTY:
			var new_dice = load("Dice/Dice.tscn").instance()
			new_dice.side = current_dice.side
			level_tilemap.add_child(new_dice)
			new_dice.global_position = level_tilemap.map_to_world(tile_position) + level_tilemap.cell_size / 2
			level_tilemap.set_cell(tile_position.x, tile_position.y, 0)
			remove_dice(dice)
			return true
	return false

func place_tile(pos: Vector2, tile_id := 0) -> void:
	var tile_position = snap_vector_to_tile(pos)
	level_tilemap.set_cell(tile_position.x, tile_position.y, tile_id)

func remove_dice(dice: DiceTextureRect) -> void:
	dice_selected = false
	dice.destroy() 
	current_dice = null
	EventBus.emit_signal("dice_unselected", {})

func _on_select_item(item: ItemTextureRect) -> void:
	if item is KeyTextureRect:
		current_item = item

func place_item(item: ItemTextureRect) -> void:
	pass

func snap_vector_to_tile(vec: Vector2) -> Vector2:
	return (vec / level_tilemap.cell_size).floor()

func _on_dice_destroyed(msg: Dictionary) -> void:
	pass

func _on_dice_unselect(dice: DiceTextureRect) -> void:
	if current_dice == dice:
		dice_selected = false
		current_dice = null
		EventBus.emit_signal("dice_unselected", {})

func _on_player_dice_holder_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.is_action_pressed("selected"):
			if is_instance_valid(current_dice):
				player.add_dice(current_dice)
				remove_dice(current_dice)
