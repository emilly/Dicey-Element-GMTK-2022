tool
class_name KeyTextureRect
extends ItemTextureRect

enum Colors {
	RED = 0
	BLUE = 1
	GREEN = 2
}

export(Colors) var color setget _set_color

var keys = [
	preload("res://Items/Key/key_red.png"),
	preload("res://Items/Key/key_blue.png"),
	preload("res://Items/Key/key_green.png"),
]

var mouse_inside := false
var selected := false

onready var animation_player := $AnimationPlayer

func _ready() -> void:
	_set_color(color)
	EventBus.connect("select_item", self, "_on_select_item")

func _on_select_item(item) -> void:
	if item == self:
		selected = true
	else:
		selected = false

func _set_color(val) -> void:
	color = val
	texture = keys[val]

func select() -> void:
	if not animation_player.is_playing():
		animation_player.play("hover")
	EventBus.emit_signal("select_item", self)

func destroy() -> void:
	EventBus.emit_signal("item_destroyed", self)
	queue_free()

func _on_KeyTextureRect_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed("selected"):
		EventBus.emit_signal("select_item", self)

func _on_KeyTextureRect_mouse_entered() -> void:
	mouse_inside = true
	if not animation_player.is_playing():
		animation_player.play("hover")

func _on_KeyTextureRect_mouse_exited() -> void:
	mouse_inside = false

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "hover" and (not mouse_inside and not selected):
		animation_player.stop()
	else:
		animation_player.play("hover")
