tool
class_name Water
extends Interactable

export var is_frozen := false setget _set_is_frozen

onready var rays := [
	$RayUp,
	$RayDown,
	$RayRight,
	$RayLeft
]

onready var sprite := $Sprite
onready var collision_shape := $CollisionShape2D
onready var area_collision_shape := $IceArea/CollisionShape2D
onready var animation_player := $AnimationPlayer

func _set_is_frozen(val) -> void:
	is_frozen = val
	if sprite:
		if is_frozen:
			sprite.frame = 1
			collision_shape.disabled = true
			area_collision_shape.disabled = false
		else:
			sprite.frame = 0
			collision_shape.disabled = false
			area_collision_shape.disabled = true

func _ready() -> void:
	_set_is_frozen(is_frozen)
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.connect("dice_unselected", self, "_on_dice_unselected")

func use_item(item: ItemTextureRect) -> bool:
	return false

func use_dice(dice: DiceTextureRect) -> bool:
	if dice.dice_resource.element == DiceResource.Element.ICE:
		freeze(dice)
		return true
	if dice.dice_resource.element == DiceResource.Element.FIRE:
		unfreeze(dice)
		return true
	return false

func get_water_nearby() -> Array:
	var water_nearby := []
	for ray in rays:
		if ray.is_colliding():
			var collider = ray.get_collider()
			if collider.is_class("Water"):
				water_nearby.push_back(collider)
	return water_nearby

func is_class(var string: String) -> bool:
	return string == "Water"

func freeze(dice: DiceTextureRect) -> void:
	is_frozen = true
	var resource = dice.dice_resource
	for water in get_water_nearby():
		if not water.is_frozen:
			if resource.side > 1:
				if resource.side != DiceResource.Special.INFINITE:
					resource.side -= 1
				water.freeze(dice)
			if resource.side == 0:
				dice.queue_free()
	animation_player.play("freeze")
	collision_shape.disabled = true
	area_collision_shape.disabled = false
	sprite.frame = 1
	EventBus.emit_signal("play_audio", "freeze")

func unfreeze(dice: DiceTextureRect) -> void:
	is_frozen = false
	var resource = dice.dice_resource
	for water in get_water_nearby():
		if water.is_frozen:
			if resource.side > 1:
				if resource.side != DiceResource.Special.INFINITE:
					resource.side -= 1
				water.freeze(dice)
			if resource.side == 0:
				dice.queue_free()
	animation_player.play("unfreeze")
	collision_shape.disabled = false
	area_collision_shape.disabled = true
	sprite.frame = 0
	EventBus.emit_signal("play_audio", "unfreeze")

func _on_dice_selected(msg: Dictionary) -> void:
	if not animation_player.is_playing():
		if msg.element == DiceResource.Element.FIRE && is_frozen:
			animation_player.play("dice_selected")
		if msg.element == DiceResource.Element.ICE && not is_frozen:
			animation_player.play("dice_selected")

func _on_dice_unselected(msg: Dictionary) -> void:
	if animation_player.current_animation == "dice_selected":
		animation_player.stop()
