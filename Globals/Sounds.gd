class_name Sounds
extends Node

onready var roll_dice_audio := $RollDiceAudio
onready var dice_selected_audio := $DiceSelectedAudio
onready var dice_used_audio := $DiceUsedAudio
onready var freeze := $FreezePlayer
onready var unfreeze := $UnfreezePlayer

func _ready() -> void:
	EventBus.connect("remaining_dice_changed", self, "_on_roll_dice")
	EventBus.connect("next_turn", self, "_on_next_turn")
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.connect("dice_used", self, "_on_dice_used")
	EventBus.connect("play_audio", self, "_on_play_audio")

func _on_roll_dice(dice, side) -> void:
	roll_dice_audio.play()

func _on_next_turn() -> void:
	pass

func _on_dice_selected(msg) -> void:
	dice_selected_audio.play()

func _on_dice_used(msg) -> void:
	dice_used_audio.play()

func _on_play_audio(audio_name) -> void:
	match audio_name:
		"freeze":
			freeze.play() if not freeze.playing else null
		"unfreeze":
			unfreeze.play() if not unfreeze.playing else null
