tool
class_name Key
extends Item

enum Colors {
	RED = 0
	BLUE = 1
	GREEN = 2
}

export(Colors) var color setget _set_color

onready var sprite := $Sprite

func _set_color(val: int) -> void:
	color = val
	if sprite:
		sprite.frame = val

func _ready() -> void:
	_set_color(color)

func _on_Key_area_entered(area: Area2D) -> void:
	if area is Player:
		EventBus.emit_signal("take_item", self)
		queue_free()
