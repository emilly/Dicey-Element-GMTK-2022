Made for GMTK-2022 "Roll of the Dice"

This is the version that was [released](https://jamker.itch.io/dicey-element) when GMTK-2022 submission period ended.

![alt text](https://img.itch.zone/aW1nLzk0NzI1MTUucG5n/315x250%23c/ekKUgl.png "Title Text")

# How to build
1. Install Godot 3.5 RC 7
2. Import the project using Godot
3. Create a folder named "build" inside the project, then create "windows", "linux", "mac" "web" folders inside the "build" folder.
4. Project -> Export -> Export all -> Release
5. Builds should be in the folder named "build"

# F.A.Q
Q. Why did you do x and not y?
A. Game was made in 2 days, code is horrible.

Q. Do i have to credit you if i use your code?
A. Not required, code is in public domain.

Q. Can i use art/music/fonts from the game?
A. You can download fonts from KennyNL's site. Music/sound effects you have to check the license case-by-case. Art isn't licensed, so you can't use it.
