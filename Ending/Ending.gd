extends Control

onready var animation_player := $AnimationPlayer

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("fast_forward"):
		animation_player.playback_speed = 4.0
	elif event.is_action_released("fast_forward"):
		animation_player.playback_speed = 1.0

func _on_Restart_button_down() -> void:
	Transition.change_scene("Levels/Level0.tscn")
