tool
class_name Dice
extends Area2D

export(Resource) var dice_resource
onready var sprite := $Sprite

onready var animation_player := $AnimationPlayer

func _set_side(_side) -> void:
	dice_resource.side = _side
	if sprite:
		sprite.frame = _side - 1

func _ready() -> void:
	animation_player.play("place")
	_set_side(dice_resource.side)

func _physics_process(delta: float) -> void:
	if Engine.editor_hint:
		if dice_resource:
			_set_side(dice_resource.side)
	else:
		set_physics_process(false)

func _on_Dice_area_entered(area: Area2D) -> void:
	pass

func _on_Dice_area_exited(area: Area2D) -> void:
	if area is Player:
		if dice_resource.side != DiceResource.Special.INFINITE:
			if dice_resource.side > 1:
				_set_side(dice_resource.side - 1)
			else:
				destroy()

func destroy() -> void:
	if get_parent() is Hole:
		get_parent().enable_collision()
	EventBus.emit_signal("dice_destroyed", {"instance": self, "position": global_position})
	animation_player.play("destroy")
	yield(animation_player, "animation_finished")
	queue_free()
