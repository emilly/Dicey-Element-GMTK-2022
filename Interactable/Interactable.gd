class_name Interactable
extends StaticBody2D

func _init() -> void:
	add_to_group("Interactable")

func use_item(item: ItemTextureRect) -> bool:
	return false

func use_dice(dice: DiceTextureRect) -> bool:
	return false
