tool
class_name Door
extends Interactable

enum Colors {
	RED = 0
	BLUE = 1
	GREEN = 2
}

export(Colors) var color setget _set_color

onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer

func _set_color(val) -> void:
	color = val
	if sprite:
		sprite.frame = val

func _ready() -> void:
	_set_color(color)
	EventBus.connect("select_item", self, "_on_select_item")
	EventBus.connect("unselect_item", self, "_on_unselect_item")

func use_item(key: ItemTextureRect) -> bool:
	if key is KeyTextureRect:
		if key.color == color:
			EventBus.emit_signal("unselect_item", key)
			key.destroy()
			destroy()
			return true
	return false

func _on_select_item(item: ItemTextureRect) -> void:
	if item is KeyTextureRect:
		if item.color == color:
			animation_player.play("select_item")

func _on_unselect_item(item: ItemTextureRect) -> void:
	animation_player.stop()
	animation_player.play("RESET")

func destroy() -> void:
	animation_player.play("destroy")
	yield(animation_player, "animation_finished")
	queue_free()
