tool
class_name DiceTextureRect
extends TextureRect

export(Resource) onready var dice_resource

var mouse_inside := false
var is_selected := false
var can_be_selected := true
export var can_be_used := true

var dice = [
	preload("res://Dice/dice_1.png"),
	preload("res://Dice/dice_2.png"),
	preload("res://Dice/dice_3.png"),
	preload("res://Dice/dice_4.png"),
	preload("res://Dice/dice_5.png"),
	preload("res://Dice/dice_6.png"),
	preload("res://Dice/dice_infinite.png")
]

var element_textures = [
	preload("res://Dice/Fire_element.png"),
	preload("res://Dice/Ice_element.png"),
	preload("res://Dice/Infinite_element.png")
]

onready var animation_player := $AnimationPlayer
onready var element_texture := $ElementTexture

func _ready() -> void:
	animation_player.play("spawn")
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	_set_side(dice_resource.side)
	_set_element_texture(dice_resource.element)

func _set_side(_side: int) -> void:
	dice_resource.side = _side
	texture = dice[_side - 1]
	rect_min_size = Vector2(64, 64)
	expand = true

func _set_element_texture(_element) -> void:
	if _element == -1:
		element_texture.texture = null
	else:
		element_texture.texture = element_textures[_element]

func _on_DiceTexture_gui_input(event: InputEvent) -> void:
	if can_be_selected:
		if event.is_action_pressed("selected"):
			EventBus.emit_signal("dice_select", {"instance": self, "side": dice_resource.side, "resource": dice_resource, "element": dice_resource.element})

func select() -> void:
	if not is_selected:
		is_selected = true
		EventBus.emit_signal("dice_select", {"instance": self, "side": dice_resource.side, "resource": dice_resource, "element": dice_resource.element})
		if not animation_player.is_playing():
			animation_player.play("hover")
	else:
		is_selected = false
		EventBus.emit_signal("dice_unselect", self)
		animation_player.play("RESET")

func _on_DiceTexture_mouse_entered() -> void:
	mouse_inside = true
	if not animation_player.is_playing():
		animation_player.play("hover")

func _on_DiceTexture_mouse_exited() -> void:
	mouse_inside = false
	if not is_selected:
		animation_player.play("RESET")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "hover":
		if not mouse_inside and not is_selected:
			animation_player.stop()
		else:
			animation_player.play("hover")
	if mouse_inside || is_selected:
		animation_player.play("hover")

func _on_dice_selected(msg) -> void:
	if msg.instance == self:
		is_selected = true
	else:
		is_selected = false

func destroy() -> void:
	can_be_selected = false
	animation_player.play("destroy")
	EventBus.emit_signal("dice_destroyed", {})
	yield(animation_player, "animation_finished")
	queue_free()
