class_name Flag
extends Area2D

func _ready() -> void:
	pass


func _on_Flag_area_entered(area: Area2D) -> void:
	if area is Player:
		EventBus.emit_signal("change_level")

func is_class(string: String) -> bool:
	return string == "Flag"
