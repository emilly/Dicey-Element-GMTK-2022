class_name DiceResource
extends Resource

enum Element {
	NONE = -1
	FIRE = 0,
	ICE = 1,
	INFINITE = 2
	STOPWATCH = 3
}

enum Special {
	INFINITE = 7
}

export(Element) var element := -1

export(int, 1, 7) var side = 1
