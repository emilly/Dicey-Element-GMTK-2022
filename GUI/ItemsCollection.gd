extends Panel

onready var key_texture := preload("res://Items/Key/KeyTextureRect.tscn")
onready var grid_container := $GridContainer

func _ready() -> void:
	EventBus.connect("take_item", self, "_on_take_item")

func _on_take_item(item) -> void:
	if item:
		var new_key_texture = key_texture.instance()
		new_key_texture.color = item.color
		grid_container.add_child(new_key_texture)
