extends Control

func _on_RollDiceButton_pressed() -> void:
	EventBus.emit_signal("roll_dice")
