extends CanvasLayer

var is_tutorial := false

onready var tutorial_label := $CenterContainer/VBoxContainer/TutorialLabel
onready var tutorial_description := $CenterContainer/VBoxContainer/TutorialDescription

func _ready() -> void:
	tutorial_description.text = ""
	EventBus.connect("tutorial_roll_dice", self, "_on_tutorial_roll_dice")
	EventBus.connect("tutorial_hole", self, "_on_hole_tutorial")
	EventBus.connect("tutorial_keys", self, "_on_keys_tutorial")
	EventBus.connect("tutorial_fire", self, "_on_fire_tutorial")
	EventBus.connect("tutorial_spikes", self, "_on_spikes_tutorial")
	EventBus.connect("tutorial_water", self, "_on_water_tutorial")
	EventBus.connect("tutorial_diagonals", self, "_on_diagonals_tutorial")

func _on_tutorial_roll_dice() -> void:
	is_tutorial = true
	visible = true
	EventBus.connect("roll_dice", self, "_on_roll_dice")

func _on_roll_dice() -> void:
	tween_text(tutorial_label, "Click on the die to select it", 2.0, 0.0)
	tween_text(tutorial_description, "You can press 1/2/3/4/5/6/7/8/9 hotkey to select a specific die", 2.0, 1.0)
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.disconnect("roll_dice", self, "_on_roll_dice")

func _on_dice_selected(msg) -> void:
	tween_text(tutorial_label, "Click on the empty die near the player to give them moves", 2.0, 0.0)
	tween_text(tutorial_description, "The amount of moves depends on the die", 2.0, 1.0)
	EventBus.connect("dice_destroyed", self, "_on_dice_destroyed")
	EventBus.disconnect("dice_selected", self, "_on_dice_selected")

func _on_dice_destroyed(msg) -> void:
	tween_text(tutorial_label, "You can move by pressing W/A/S/D", 2.0, 0.0)
	EventBus.disconnect("dice_destroyed", self, "_on_dice_destroyed")

func _on_hole_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "Do you see that hole? You can actually put a dice there!", 2.0, 0.0)
	EventBus.connect("dice_used", self, "_on_hole_dice_used")

func _on_hole_dice_used(msg) -> void:
	if msg && msg.instance is Hole:
		tween_text(tutorial_label, "When you're good, you're good.", 2.0, 0.0)

func _on_keys_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "Collect the red key", 2.0, 0.0)
	tween_text(tutorial_description, "I hope you're not color blind, if you are, I'm sorry.", 2.0, 1.0)
	EventBus.connect("take_item", self, "_on_keys_take_item")

func _on_keys_take_item(item) -> void:
	if item is Key:
		tween_text(tutorial_label, "Great! Now you can open the door", 2.0, 0.0)
		tween_text(tutorial_description, "Click on the key inside an inventory and then click on the highlighted door", 2.0, 1.0)
	EventBus.connect("item_destroyed", self, "_on_keys_item_destroyed")
	
func _on_keys_item_destroyed(item) -> void:
	if item is KeyTextureRect:
		tween_text(tutorial_label, "You can Restart the level by pressing R", 2.0, 0.0)

func _on_fire_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "Do you see a fire on a die? It can burn wood!", 2.0, 0.0)
	tween_text(tutorial_description, "Okay, it might not look like wood, but you get the point.", 2.0, 1.0)

func _on_spikes_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "Don't stand on red spikes!", 2.0, 0.0)
	tween_text(tutorial_description, "Step aside.", 2.0, 1.0)

func _on_water_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "You can freeze and unfreeze the water", 2.0, 0.0)
	tween_text(tutorial_description, "Don't you love sliding on ice? If only the door wasn't in the way", 2.0, 1.0)

func tween_text(label: Label, text: String, duration := 2.0, delay := 0.0) -> void:
	label.text = text
	label.percent_visible = 0.0
	var tween = create_tween()
	tween.set_ease(Tween.EASE_IN_OUT)
	tween.set_trans(Tween.TRANS_CUBIC)
	tween.tween_property(label, "percent_visible", 1.0, 1.0).from(0.0).set_delay(delay)
	tween.play()

func _on_diagonals_tutorial() -> void:
	is_tutorial = true
	visible = true
	tween_text(tutorial_label, "You can move diagonally", 2.0, 0.0)
	tween_text(tutorial_description, "Press both directions at the same time", 2.0, 1.0)
