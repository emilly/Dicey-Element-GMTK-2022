tool
class_name Spikes
extends Area2D

export var activated := true setget _set_activated

onready var collision_shape := $CollisionShape2D as CollisionShape2D
onready var animation_player := $AnimationPlayer as AnimationPlayer

func _set_activated(val) -> void:
	activated = val
	if animation_player:
		if activated:
			collision_shape.disabled = false
			animation_player.play("activate")
		else:
			collision_shape.disabled = true
			animation_player.play("deactivate")

func _ready() -> void:
	EventBus.connect("next_turn", self, "_on_next_turn")
	if activated:
		collision_shape.disabled = false
		animation_player.play("activate")
	else:
		collision_shape.disabled = true
		animation_player.play("deactivate")

func _on_next_turn() -> void:
	activated = !activated
	if activated:
		collision_shape.disabled = false
		animation_player.play("activate")
		return
	collision_shape.disabled = true
	animation_player.play("deactivate")


func _on_Spikes_area_entered(area: Area2D) -> void:
	if area is Player:
		area.die()
