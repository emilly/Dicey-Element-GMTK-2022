class_name Wood
extends Interactable

var is_burning := false

onready var rays := [
	$RayUp,
	$RayDown,
	$RayRight,
	$RayLeft
]

onready var animation_player := $AnimationPlayer

func _ready() -> void:
	EventBus.connect("dice_selected", self, "_on_dice_selected")
	EventBus.connect("dice_unselected", self, "_on_dice_unselected")

func use_item(item: ItemTextureRect) -> bool:
	return false

func use_dice(dice: DiceTextureRect) -> bool:
	if dice.dice_resource.element == DiceResource.Element.FIRE:
		var dice_instance = dice.duplicate()
		dice_instance.dice_resource = dice_instance.dice_resource.duplicate()
		self.catch_fire(dice_instance.duplicate())
		return true
	return false

func get_wood_nearby() -> Array:
	var wood_nearby := []
	for ray in rays:
		if ray.is_colliding():
			var collider = ray.get_collider()
			if collider.is_class("Wood"):
				wood_nearby.push_back(collider)
	return wood_nearby

func is_class(var string: String) -> bool:
	return string == "Wood"

func catch_fire(dice: DiceTextureRect) -> void:
	is_burning = true
	var resource = dice.dice_resource
	for wood in get_wood_nearby():
		if not wood.is_burning:
			if resource.side > 1:
				resource.side -= 1
				wood.catch_fire(dice)
			if resource.side == 0:
				dice.queue_free()
	animation_player.play("catch_fire")
	yield(animation_player, "animation_finished")
	queue_free()

func _on_dice_selected(msg) -> void:
	if not is_burning:
		if msg.element == DiceResource.Element.FIRE:
			animation_player.play("dice_selected")

func _on_dice_unselected(msg) -> void:
	if not is_burning:
		animation_player.play("RESET")
