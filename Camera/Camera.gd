class_name LevelCamera
extends Camera2D

onready var default_zoom := zoom
onready var default_global_position := global_position

func tween_in(player_pos: Vector2, duration: float) -> void:
	var tween = create_tween()
	tween.set_parallel(true)
	tween.tween_property(self, "zoom", Vector2(0, 0), duration)
	tween.tween_property(self, "position", player_pos, duration)

func tween_out(player_pos: Vector2, duration: float) -> void:
	var tween = create_tween()
	zoom = Vector2(0.05, 0.05)
	tween.set_parallel(true)
	tween.set_ease(Tween.EASE_IN_OUT)
	tween.set_trans(Tween.TRANS_SINE)
	tween.tween_property(self, "zoom", default_zoom, duration)
	tween.tween_property(self, "position", default_global_position, duration)

func reset(player_position: Vector2) -> void:
	zoom = Vector2(0.05, 0.05)
	position = player_position - Vector2(-8, 8)
