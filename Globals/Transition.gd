extends CanvasLayer

onready var color_rect := $ColorRect
onready var animation_player := $AnimationPlayer

onready var audio := $AudioStreamPlayer

func change_scene(path: String) -> void:
	change_scene_to(load(path))

func change_scene_to(packed_scene: PackedScene) -> void:
	var player := get_player()
	var viewport_pos
	if player:
		audio.play()
		viewport_pos = get_viewport_pos(player)
		if viewport_pos != Vector2.ZERO:
			color_rect.material.set_shader_param("pos", viewport_pos)
	else:
		color_rect.material.set_shader_param("pos", Vector2(0.5, 0.5))
	animation_player.play("transition_in")
	
	yield(animation_player, "animation_finished")
	get_tree().change_scene_to(packed_scene)
	
	yield(get_tree(), "idle_frame")
	
	player = get_player()
	var camera := get_camera()
	if camera:
		camera.reset(player.position)
	if player:
		viewport_pos = get_viewport_pos(player)
		if viewport_pos != Vector2.ZERO:
			color_rect.material.set_shader_param("pos", Vector2(0.4, 0.7))
	else:
		color_rect.material.set_shader_param("pos", Vector2(0.5, 0.5))
	animation_player.play("transition_out")
	yield(animation_player, "animation_finished")
	if camera:
		camera.tween_out(player.position, 2.0)

func reload_current_scene() -> void:
	var player := get_player()
	var viewport_pos = get_viewport_pos(player)
	if viewport_pos != Vector2.ZERO:
		color_rect.material.set_shader_param("pos", viewport_pos)
	animation_player.play("transition_in")
	
	yield(animation_player, "animation_finished")
	get_tree().reload_current_scene()
	
	yield(get_tree(), "idle_frame")
	player = get_player()
	var camera := get_camera()
	camera.reset(player.position)
	viewport_pos = get_viewport_pos(player)
	if viewport_pos != Vector2.ZERO:
		color_rect.material.set_shader_param("pos", Vector2(0.4, 0.7))
	animation_player.play("transition_out")
	yield(animation_player, "animation_finished")
	camera.tween_out(player.global_position, 2.0)

func get_viewport_pos(player: Player) -> Vector2:
	if not player:
		return Vector2(0.5, 0.5)
	var screen_pos = player.raycast.get_global_transform_with_canvas().origin
	var viewport_pos = Vector2(screen_pos.x / color_rect.rect_size.x, screen_pos.y / color_rect.rect_size.y)
	return viewport_pos                     

func get_camera() -> LevelCamera:
	var camera = get_tree().get_nodes_in_group("Camera")
	return camera[0] if camera else null

func get_player() -> Player:
	var player = get_tree().get_nodes_in_group("Player")
	return player[0] if player else null
