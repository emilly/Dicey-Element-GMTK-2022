extends Node

signal roll_dice
signal remaining_dice_changed(dice, side)
signal dice_select(msg)
signal dice_selected(msg)
signal dice_unselect(msg)
signal dice_unselected(msg)
signal dice_destroyed(msg)
signal item_destroyed(item)
signal take_item(item)
signal select_item(item)
signal unselect_item(item)
signal change_level
signal next_turn()
signal dice_used(msg)

signal tutorial_roll_dice
signal tutorial_hole
signal tutorial_water
signal tutorial_fire
signal tutorial_keys
signal tutorial_spikes
signal tutorial_diagonals

signal play_audio(audio_name)
